# Daily planner




What the application is about:
This application is designed to assist users in effectively managing their time and mental resources. Currently, there are three available commands:

1. /addEvent {event} {date} - Allows users to add a new event to their schedule.
   Example:
   /addEvent Basketball Practice 2022-01-10 11:20

2. /showEvents - Displays all the events in the user's schedule.

3. /deleteEvents {substring} - Deletes all events that contain the specified substring.
   Example:
   /deleteEvents Basketball - This command will delete all events with the word "Basketball" in their description.


(I note that the application already has a chatbot and the token is publicly available, so you can immediately switch to the bot
without additional registration (@super_daily_planner_bot (https://t.me/super_daily_planner_bot)))




## Getting started

To start a project, you need to do 3 things:
1) run the `sbt` file
2) fill `application.conf` like : 
` mydb = {
  driver = "com.mysql.cj.jdbc.Driver",
  url = "jdbc:mysql://localhost:3306/daily?serverTimezone=UTC",
  user = "admin",
  password = "sqlsql",
  connectionPool = disabled
}`

3) run a couple of commands from the list below.

# Comamnds

1) You can download a specific version or opt for the latest release as seen in the following command:
`sudo docker pull mysql/mysql-server:latest`
 2)  Verify the image is now stored locally by listing the downloaded Docker images:
`sudo docker run -d -p 3306:3306 --name mysql-docker-container -e MYSQL_ROOT_PASSWORD=sqlsql -e MYSQL_DATABASE=daily -e MYSQL_USER=admin -e MYSQL_PASSWORD=sqlsql mysql/mysql-server:latest`

3) go to the shall
`sudo docker exec -it mysql-docker-container bash`

4) sign in
`mysql -u root -p`
`sqlsql`

5) use new database  
`USE daily;`
6) create table: 

CREATE TABLE `user_data` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`user_id` int(10) unsigned NOT NULL,
`text` varchar(256) DEFAULT NULL,
`data` varchar(256) NOT NULL,
PRIMARY KEY (`id`),
KEY `user_id` (`user_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 DEFAULT COLLATE = utf8_general_ci;