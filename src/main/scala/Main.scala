import telegram_bot.TelegramBot
import telegram_bot.config.TestDockerConfigBuilder
import telegram_bot.controller.TelegramBotController
import telegram_bot.dao.TelegramUsersDao
import telegram_bot.handler.DailyPlannerBot
import telegram_bot.scheduler.TelegramBotScheduler
import zio._

object Main extends ZIOAppDefault {

  def run: ZIO[ZIOAppArgs with Scope, Any, Any] = {
    program
      .provideSome[Scope with ZIOAppArgs](makeEnv)
      .provideLayer(ZLayer.service[Scope] ++ ZLayer.service[ZIOAppArgs])
      .mapErrorCause { cause =>
        Cause.fail(withoutInterruptions(cause.untraced).prettyPrint)
      }
  }

  private def withoutInterruptions(cause: Cause[_]): Cause[_] = {
    cause.fold[Cause[_]](
      empty0 = Cause.empty,
      failCase0 = (f, trace) => Cause.fail(f).traced(trace),
      dieCase0 = (d, trace) => Cause.die(d).traced(trace),
      interruptCase0 = (_, _) => Cause.empty
    )(
      thenCase0 = (c1, c2) => if (c1.isInterruptedOnly) c2 else if (c2.isInterruptedOnly) c1 else Cause.Then(c1, c2),
      bothCase0 = (c1, c2) => if (c1.isInterruptedOnly) c2 else if (c2.isInterruptedOnly) c1 else Cause.Both(c1, c2),
      stacklessCase0 = (cause, _) => cause
    )
  }

  private def makeEnv: ZLayer[Scope with ZIOAppArgs, Throwable, TelegramBot] =
    ZLayer.makeSome[Scope with ZIOAppArgs, TelegramBot](
      ZLayer.succeed(TestDockerConfigBuilder.createDatabase("", "")),
      TelegramUsersDao.observedLive,
      ZLayer.succeed("6713539630:AAFa5zrci7q2tEY2I9-IOsU9QhbORjjs2z8"),
      DailyPlannerBot.live,
      TelegramBotScheduler.live,
      TelegramBotController.live,
      TelegramBot.live
    )

  private def program: ZIO[TelegramBot, Throwable, Any] =
    for {
      telegramBot <- ZIO.service[TelegramBot]
      _ <- telegramBot.run
    } yield ()

}
