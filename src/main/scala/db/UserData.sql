CREATE TABLE `user_data`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(10) unsigned NOT NULL,
    `text`    varchar(256) DEFAULT NULL,
    `data`    varchar(256) NOT NULL,
    PRIMARY KEY (`id`),
    KEY       `user_id` (`user_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 DEFAULT COLLATE = utf8_general_ci;
