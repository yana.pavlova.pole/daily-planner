package telegram_bot

import telegram_bot.controller.TelegramBotController
import zio.IO

class TelegramBotImpl(telegramBotController: TelegramBotController) extends TelegramBot {

  override def run: IO[Throwable, Unit] =
    for {
      _ <- telegramBotController.execute
    } yield ()
}
