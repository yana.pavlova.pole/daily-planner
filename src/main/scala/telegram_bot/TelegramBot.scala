package telegram_bot

import telegram_bot.controller.TelegramBotController
import zio.{Task, ZLayer}

trait TelegramBot {
  def run: Task[Unit]
}

object TelegramBot {

  val live: ZLayer[TelegramBotController, Nothing, TelegramBot] =
    ZLayer.fromFunction(new TelegramBotImpl(_))
}
