package telegram_bot.scheduler

import zio.{IO, ZLayer}

trait TelegramBotScheduler {
  def schedule: IO[Throwable, Unit]
}

object TelegramBotScheduler {
  val live = ZLayer.fromFunction(new TelegramBotSchedulerLive(_))
}
