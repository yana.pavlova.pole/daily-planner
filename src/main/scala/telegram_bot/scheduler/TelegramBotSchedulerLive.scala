package telegram_bot.scheduler

import telegram_bot.dao.TelegramUsersDao
import zio.{IO, ZIO}

class TelegramBotSchedulerLive(userDao: TelegramUsersDao) extends TelegramBotScheduler {

  override def schedule: IO[Throwable, Unit] = {
    Console.print("hello from TelegramBotSchedulerLive")
    ZIO.unit
  }
}
