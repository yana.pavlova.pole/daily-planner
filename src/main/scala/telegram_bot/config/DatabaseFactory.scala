package telegram_bot.config

import slick.jdbc.{DriverDataSource, JdbcBackend}
import slick.jdbc.JdbcBackend.Database
import slick.util.AsyncExecutor

object DatabaseFactory {

  def create(
      config: DatabaseConfig,
      executor: AsyncExecutor = AsyncExecutor.default()): JdbcBackend.DatabaseDef = {
    val db = Database.forDataSource(
      ds = new DriverDataSource(config.masterUrl, user = config.username, password = config.password),
      maxConnections = Some(30),
      executor = executor
    )
    sys.addShutdownHook {
      db.close()
    }
    db
  }

}
