package telegram_bot.config

import org.testcontainers.containers.MySQLContainer
import org.testcontainers.utility.DockerImageName
import slick.jdbc.JdbcBackend.{Database, DatabaseDef}

object TestDockerConfigBuilder {

  private val dockerImageName = DockerImageName
    .parse("mysql:8.0")
    .asCompatibleSubstituteFor("mysql")
  class TestMySQLContainer extends MySQLContainer[TestMySQLContainer](dockerImageName)

  val container = new TestMySQLContainer()

  private def containerUrl = container.getJdbcUrl + "?useSSL=false"

  private def createAndStartContainer(containerName: String, schemaFilename: String): Unit = {
    container
      .withCreateContainerCmdModifier { cmdModifier =>
        cmdModifier
          .withCmd("--datadir=/tmpfs")
          .withCmd("--character-set-server=utf8mb4")
          .withCmd("--sql-mode=NO_ENGINE_SUBSTITUTION")
      }
      .withDatabaseName(containerName)
      .withUsername("admin")
      .withPassword("sqlsql")
      .withReuse(true)
      .withInitScript(schemaFilename)
      .start()
  }

  def createDatabase(dbName: String, schemaFileName: String): DatabaseDef = {
//    createAndStartContainer(dbName, schemaFileName)

    val db = Database.forConfig("mydb")
    db
  }

}
