package telegram_bot.config

import com.typesafe.config.Config

case class DatabaseConfig(
    masterUrl: String,
    username: String,
    password: String)

object DatabaseConfig {

  def apply(config: Config): DatabaseConfig = {
    new DatabaseConfig(
      masterUrl = config.getString("master-url"),
      username = config.getString("username"),
      password = config.getString("password")
    )
  }
}
