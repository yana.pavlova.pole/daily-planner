package telegram_bot.dao

case class UserEvent(id: Int = 0, userId: String, text: Option[String], data: String)
