package telegram_bot.dao

import com.google.common.util.concurrent.MoreExecutors
import slick.jdbc.JdbcBackend
import zio.{ZIO, ZLayer}

import scala.concurrent.{ExecutionContext, Future}

class TelegramUsersDao(database: JdbcBackend.DatabaseDef) {

  implicit private val ec: ExecutionContext = ExecutionContext.fromExecutor(MoreExecutors.sameThreadExecutor())

  private def throwIfNothingChanged(cnt: Int): Future[Unit] =
    if (cnt > 0) Future.unit
    else Future.failed(new NoSuchElementException(s"no such elements"))

  def addEvent(userEvent: UserEvent): Future[Unit] =
    database.run(AggregatorsOperations.addEvent(userEvent)).map(_ => ())

  def deleteEvent(userEvent: UserEvent): Future[Unit] =
    database.run(AggregatorsOperations.deleteEvent(userEvent)).map(throwIfNothingChanged(_))

  def selectEvent(userEvent: UserEvent): Future[Option[UserEvent]] = ???

  def selectAllEvents: Future[Seq[UserEvent]] = database.run(AggregatorsOperations.selectAll)
}

object TelegramUsersDao {

  val observedLive: ZLayer[JdbcBackend.DatabaseDef, Nothing, TelegramUsersDao] = ZLayer.fromZIO {
    for {
      config <- ZIO.service[JdbcBackend.DatabaseDef]
    } yield new TelegramUsersDao(config)
  }
}
