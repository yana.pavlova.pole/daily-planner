package telegram_bot.dao

import slick.jdbc.{GetResult, MySQLProfile, PositionedResult}

object AggregatorsOperations {

  import MySQLProfile.api._
  import Schema._

  implicit private object GetAggregator extends GetResult[UserEvent] {

    override def apply(v: PositionedResult): UserEvent = {
      UserEvent(id = v.<<, userId = v.<<, text = v.<<, data = v.<<)
    }
  }

  private val columnsList: String =
    s"""
    ${UserDataTable.Columns.Id},
    ${UserDataTable.Columns.UserId},
    ${UserDataTable.Columns.Text},
    ${UserDataTable.Columns.Data}"""

  def addEvent(userEvent: UserEvent): DBIOAction[Int, NoStream, Effect] = {
    // можно добавить `ON DUPLICATE KEY UPDATE`
    val userId = userEvent.userId.toLong
    val text = userEvent.text.getOrElse("")
    val data = userEvent.data
    sqlu"""
            INSERT INTO  #${UserDataTable.Name}
            (#${UserDataTable.Columns.UserId},
            #${UserDataTable.Columns.Text} ,
            #${UserDataTable.Columns.Data} )
            VALUES ($userId, $text, $data);
         """

  }

  def deleteEvent(userEvent: UserEvent): DBIOAction[Int, NoStream, Effect] = {
    val userId = userEvent.userId.toLong
    val text = userEvent.text
    sqlu"""
            DELETE FROM  #${UserDataTable.Name}
            WHERE #${UserDataTable.Columns.UserId}=$userId AND #${UserDataTable.Columns.Text} LIKE CONCAT('%', $text, '%')
         """
  }

  def selectAll: DBIOAction[Seq[UserEvent], NoStream, Effect] = {
    sql"""
        SELECT
        #$columnsList
        FROM #${UserDataTable.Name}
     """.as[UserEvent]
  }

}
