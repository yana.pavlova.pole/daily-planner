package telegram_bot.dao

object Schema {

  object UserDataTable {

    val Name = "user_data"

    object Columns {
      val Id: String = "id"
      val UserId: String = "user_id"
      val Text: String = "text"
      val Data: String = "data"
    }

  }

}
