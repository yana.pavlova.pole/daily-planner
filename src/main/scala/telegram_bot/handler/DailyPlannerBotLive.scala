package telegram_bot.handler

import com.bot4s.telegram.api.declarative.{Callbacks, Commands}
import com.bot4s.telegram.future.Polling
import com.bot4s.telegram.methods._
import com.bot4s.telegram.models._
import telegram_bot.dao.{TelegramUsersDao, UserEvent}
import zio.{IO, ZIO}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Failure

class DailyPlannerBotLive(token: String, userDao: TelegramUsersDao)
  extends ExampleBot(token)
  with DailyPlannerBot
  with Polling
  with Commands[Future]
  with Callbacks[Future] {

  override def route(): IO[Throwable, Unit] = {
    ZIO.succeed(run())
  }

  private var pollMsgId = 0

  private def sendMessage(message: SendMessage): Future[Unit] = {
    val f = request(message)
    f.onComplete {
      case Failure(e) => println("Error " + e)
      case _ =>
    }
    for {
      poll <- f
    } yield {
      println("some message was sent")
      pollMsgId = poll.messageId
    }
  }

  onCommand("addEvent") { implicit msg =>
    val f1 = SendMessage(ChatId(msg.chat.id), s"Information about event: ${msg.text}, was added in list")
    val f2 = userDao.addEvent(
      UserEvent(
        0,
        msg.chat.id.toString,
        msg.text.map(_.split(" ")).map(strs => strs.takeRight(strs.length - 1).foldLeft("")(_ ++ _)),
        msg.text.getOrElse("").split(" ").last
      )
    )
    Await.result(f2, Duration(5, TimeUnit.SECONDS))
    for {
      _ <- f2
      _ <- sendMessage(f1)
    } yield ()
  }

  onCommand("showEvents") { implicit msg =>
    val f2 = userDao.selectAllEvents
    for {
      allUserEvents <- f2
      _ <- sendMessage(
        SendMessage(
          ChatId(msg.chat.id),
          "All your plans:" + allUserEvents
            .map(userEvent => s"\n plan: ${userEvent.text.getOrElse("")}")
            .foldLeft("")(_ ++ _)
        )
      )
    } yield ()
  }

  onCommand("deleteEvents") { implicit msg =>
    val f2 = userDao.deleteEvent(
      UserEvent(
        0,
        msg.chat.id.toString,
        msg.text.map(_.split(" ")).map(strs => strs.takeRight(strs.length - 1).foldLeft("")(_ ++ _)),
        msg.text.getOrElse("").split(" ").last
      )
    )
    for {
      _ <- f2
      _ <- sendMessage(SendMessage(ChatId(msg.chat.id), s"Event: ${msg.text}, was deleted"))
    } yield ()
  }
}
