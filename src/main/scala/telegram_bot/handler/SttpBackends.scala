package telegram_bot.handler

import sttp.client3.SttpBackend
import sttp.client3.okhttp.OkHttpFutureBackend

import scala.concurrent.Future

object SttpBackends {
  val default: SttpBackend[Future, Any] = OkHttpFutureBackend()
}
