package telegram_bot.handler

import zio.{IO, ZLayer}

trait DailyPlannerBot {
  def route(): IO[Throwable, Unit]
}

object DailyPlannerBot {

  val live =
    ZLayer.fromFunction(new DailyPlannerBotLive(_, _))
}
