package telegram_bot.handler

import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.clients.FutureSttpClient
import com.bot4s.telegram.future.TelegramBot
import sttp.client3.SttpBackend

import scala.concurrent.Future

/** Generates random values.
 */
abstract class ExampleBot(val token: String) extends TelegramBot {

  implicit val backend: SttpBackend[Future, Any] = SttpBackends.default
  override val client: RequestHandler[Future] = new FutureSttpClient(token)
}
