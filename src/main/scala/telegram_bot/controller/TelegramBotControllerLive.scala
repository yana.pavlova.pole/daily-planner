package telegram_bot.controller

import telegram_bot.handler.DailyPlannerBotLive
import zio.{IO, ZIO, _}

class TelegramBotControllerLive(bot: DailyPlannerBotLive) extends TelegramBotController {

  override def execute: IO[Throwable, Unit] =
    for {
      _ <- bot.route()
      _ <- Console.readLine
      _ <- ZIO.attempt(bot.shutdown()).uninterruptible
    } yield ()

}
