package telegram_bot.controller

import zio.{IO, ZLayer}

trait TelegramBotController {
  def execute: IO[Throwable, Unit]
}

object TelegramBotController {
  val live = ZLayer.fromFunction(new TelegramBotControllerLive(_))
}
