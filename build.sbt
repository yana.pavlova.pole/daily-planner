ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"




lazy val root = (project in file("."))
  .settings(
    name := "Main"
  )

libraryDependencies += "dev.zio" %% "zio" % "2.0.19"
libraryDependencies += "dev.zio" %% "zio-streams" % "2.0.19"
libraryDependencies += "dev.zio" %% "zio-macros" % "2.0.19"


libraryDependencies += "dev.zio" %% "zio" % "2.0.19"
libraryDependencies += "dev.zio" %% "zio-streams" % "2.0.19"

libraryDependencies += "com.bot4s" %% "telegram-core" % "5.7.1"
libraryDependencies += "com.bot4s" %% "telegram-akka" % "5.7.1"

libraryDependencies += "biz.enef" %% "slogging" % "0.6.2"

libraryDependencies += "com.softwaremill.sttp.client3" %% "core" % "3.9.1"
libraryDependencies += "com.softwaremill.sttp.client3" %% "okhttp-backend-monix" % "3.9.1"



libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.12"
libraryDependencies += "com.mysql" % "mysql-connector-j" % "8.0.33"
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.4.1"

libraryDependencies += "com.google.guava" % "guava" % "17.0"

libraryDependencies += "org.testcontainers" % "testcontainers" % "1.17.3"
libraryDependencies += "org.testcontainers" % "mysql" % "1.17.3"